
OpenBlock.Env.registerNativeTypes(['web3d'], {
    'THREE.Object3D': [],
    'THREE.Vector3': [],
    'THREE.BufferGeometry': [],
    'THREE.BoxGeometry': ['THREE.BufferGeometry'],
    'THREE.Mesh': ['THREE.Object3D'],
    'THREE.Material': [],
    'THREE.MeshStandardMaterial': ['THREE.Material']
});
OpenBlock.Env.registerEvents(['web3d'], '', [
    { "name": "click", "argType": "THREE.Vector3" },
    { "name": "touchstart", "argType": "THREE.Vector3" },
    { "name": "touchmove", "argType": "THREE.Vector3" },
    { "name": "touchcancel", "argType": "THREE.Vector3" },
    { "name": "touchend", "argType": "THREE.Vector3" },
    { "name": "mousemove", "argType": "THREE.Vector3" },
    { "name": "longpress", "argType": "THREE.Vector3" },
    { "name": "swipe" },
    { "name": "windowResize" },
    { "name": "keydown", "argType": "String" },
    { "name": "keyup", "argType": "String" },
]);
OpenBlock.Env.registerNativeFunction(['web3d'], 'three.js', 'three.js', [
    {
        method_name: 'THREE.Object3D_get_position', arguments: [
            { type: 'THREE.Object3D', name: 'object' }
        ], returnType: 'THREE.Vector3'
    },
    {
        method_name: 'THREE.Vector3_set_x', arguments: [
            { type: 'THREE.Vector3', name: 'object' },
            { type: 'Number', name: 'x' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.Vector3_set_y', arguments: [
            { type: 'THREE.Vector3', name: 'object' },
            { type: 'Number', name: 'y' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.Vector3_set_z', arguments: [
            { type: 'THREE.Vector3', name: 'object' },
            { type: 'Number', name: 'z' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.BoxGeometry_new', arguments: [
        ], returnType: 'THREE.BoxGeometry'
    },
    {
        method_name: 'THREE.Scene_add', arguments: [
            { type: 'THREE.Object3D', name: 'object' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.Mesh_new', arguments: [
            { type: 'THREE.BufferGeometry', name: 'geometry' },
            { type: 'THREE.Material', name: 'material' },
        ], returnType: 'THREE.Mesh'
    },
    {
        method_name: 'THREE.MeshStandardMaterial_new', arguments: [
            { type: 'Colour', name: 'color' },
        ], returnType: 'THREE.MeshStandardMaterial'
    },
    {
        method_name: 'THREE.build_box_at', arguments: [
            { type: 'Colour', name: 'color' },
            { type: 'Number', name: 'x' },
            { type: 'Number', name: 'y' },
            { type: 'Number', name: 'z' },
        ], returnType: 'THREE.Mesh'
    },
    {
        method_name: 'THREE.Mesh_set_color', arguments: [
            { type: 'THREE.Mesh', name: 'mesh' },
            { type: 'Colour', name: 'color' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.Mesh_set_scale', arguments: [
            { type: 'THREE.Mesh', name: 'mesh' },
            { type: 'Number', name: 'x' },
            { type: 'Number', name: 'y' },
            { type: 'Number', name: 'z' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.build_box_at_v3', arguments: [
            { type: 'Colour', name: 'color' },
            { type: 'THREE.Vector3', name: 'position' },
        ], returnType: 'THREE.Mesh'
    },
    {
        method_name: 'THREE.Vector3_get_x', arguments: [
            { type: 'THREE.Vector3', name: 'position' },
        ], returnType: 'Number'
    },
    {
        method_name: 'THREE.Vector3_get_y', arguments: [
            { type: 'THREE.Vector3', name: 'position' },
        ], returnType: 'Number'
    },
    {
        method_name: 'THREE.Vector3_get_z', arguments: [
            { type: 'THREE.Vector3', name: 'position' },
        ], returnType: 'Number'
    },
    {
        method_name: 'THREE.Scene_remove', arguments: [
            { type: 'THREE.Object3D', name: 'object' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.Mesh_set_opacity', arguments: [
            { type: 'THREE.Mesh', name: 'mesh' },
            { type: 'Number', name: 'opacity' },
        ], returnType: 'void'
    },
    {
        method_name: 'THREE.Mesh_get_opacity', arguments: [
            { type: 'THREE.Mesh', name: 'mesh' },
        ], returnType: 'void'
    },
]);