let h = Vue.h;
let UIBuilderRT = Vue.defineAsyncComponent(async () => {
    return {
        props: { page: { type: Object } },
        render() {
            if (this.page) {
                return h('div', {}, [h('span', {}, ['data'])]);
            } else {
                return h('div', {}, [h('span', {}, ['Empty'])]);
            }
        },
        data() {
            return {
            };
        }
    };
});
export default UIBuilderRT;